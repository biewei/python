# -*- coding:utf-8 -*-
"""
图片批量压缩并替换原图片
输入目录绝对路径
"""
import cv2
import os
import numpy as np
import imutils


# ## 读取图像 + 中文处理
def cv_imread(filePath):
    cv_img = cv2.imdecode(np.fromfile(filePath, dtype=np.uint8), -1)
    return cv_img


# ## 保存图片 + 中文处理
def cv_imwrite(filename, src):
    cv2.imencode('.jpg', src)[1].tofile(filename)


# ## 压缩图片
def yasuo(fpath, real_name):
    path = os.path.join(fpath, real_name)
    fileList = os.listdir(path)  # ## 读取文件列表
    for name in fileList:
        fname = os.path.join(path, name)
        if os.path.isdir(fname) == True:
            continue
        fix = name.split('.')[1]
        lower_fix = fix.lower()
        if lower_fix != "jpg" and lower_fix != "png" and lower_fix != "jpeg":
            continue
        image = cv_imread(fname)
        (h, w, n) = image.shape
        if w > 1000:
            image = imutils.resize(image, width=1000)
            cv_imwrite(fname, image)


if __name__ == "__main__":
    path = str(input("Please input file path >"))
    if os.path.isdir(path) != True:
        exit("请输入正确的目录地址！")
    i = 0
    for user in (os.listdir(path)):
        yasuo(path, user)
        i += 1
        print(i, " >>> ", user, " . . . The image compress success . . .")
